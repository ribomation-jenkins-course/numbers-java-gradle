package ribomation.numbers.result;

import ribomation.numbers.func.Function;

import java.math.BigInteger;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementMap;
import org.simpleframework.xml.Root;

/**
 * Encapsulates the result for one argument
 *
 * @user jens
 * @date 2015-05-03
 */
@Root(name = "computation")
public class Result {
    /**
     * The argument all for all function values.
     */
    @Attribute
    private Integer argument = 0;

    /**
     * Contains a timed result for each computed function
     */
    @ElementMap(entry = "function", key = "name", attribute = true, inline = true)
    private Map<String, Value> values = new TreeMap<>();

    /**
     * Adds a computed result
     *
     * @param name    function name
     * @param result  result given some argument
     * @param elapsed elapsed time in milli-secs
     */
    public void add(String name, BigInteger result, double elapsed) {
        Value value = new Value(name, result, elapsed);
        values.put(value.name, value);
    }

    /**
     * Takes an argument and a bunch of functions, and computes all results.
     *
     * @param argument  the argument to apply for each function
     * @param functions bunch of functions
     * @return result
     */
    public Result compute(Integer argument, List<Function> functions) {
        this.argument = argument;
        for (final Function f : functions) {
            StopWatch sw = new StopWatch();
            BigInteger result = sw.time(new StopWatch.TimedExpression<BigInteger>() {
                @Override
                public BigInteger compute() {
                    return f.compute(getArgument());
                }
            });
            add(f.getName(), result, sw.elapsedMilliSecs());
        }
        return this;
    }

    public Integer getArgument() {
        return argument;
    }

    public Map<String, Value> getValues() {
        return values;
    }

    @Override
    public String toString() {
        return "Result{" +
                "input=" + argument +
                ", " + values +
                '}';
    }

    public String toCSV() {
        return toCSV(";");
    }

    public String toCSV(final String SEP) {
        StringBuilder buf = new StringBuilder(1000);
        buf.append(argument);
        for (Value v : values.values()) {
            buf.append(SEP);
            buf.append(v.name);
            buf.append(SEP);
            buf.append(v.result);
            buf.append(SEP);
            buf.append(v.elapsedMilliSecs);
        }
        return buf.toString();
    }
}
