package ribomation.numbers.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Action for output formats
 *
 * @user jens
 * @date 2015-05-04
 */
public class UpdateFormat implements ActionListener {
    private ResultsFormat fmt;
    private Configuration configuration;

    public UpdateFormat(ResultsFormat fmt, Configuration configuration) {
        this.fmt = fmt;
        this.configuration = configuration;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        configuration.format = fmt;
    }
}
