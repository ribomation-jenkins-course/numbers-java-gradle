package ribomation.numbers.gui;

/**
 * Output format configuration
 *
 * @user jens
 * @date 2015-05-04
 */
public class Configuration {
    boolean       list   = false;
    ResultsFormat format = ResultsFormat.plain;

    @Override
    public String toString() {
        return "Configuration{" +
                "list=" + list +
                ", format=" + format +
                '}';
    }
}
